$(document).ready(function() {

	/* DataTables */

	$('#tb-danhsachthuoc').DataTable({
		"lengthChange" : false,
		"searching" : false,
		// disable showing page entries
		"info" : false,
		/* full button: first...last */
		"pagingType" : "full_numbers",
		"scrollY" : "450px",
		/* Custom icon button paging */
		language : {
			paginate : {
				"first" : '<i class="fas fa-fast-backward"></i>',
				"previous" : '<i class="fas fa-step-backward"></i>',
				"next" : '<i class="fas fa-step-forward"></i>',
				"last" : '<i class="fas fa-fast-forward"></i>'
			}
		}
	});

	/* toggle icon sidebar */
	$(".toggle-search-1").click(function() {
		$("#searchBar-1").toggle(300);
		$(".icon-change-1").toggleClass('fa-minus-circle fa-plus-circle');
	});
	$(".toggle-search-2").click(function() {
		$(".icon-change-2").toggleClass('fa-minus-circle fa-plus-circle');
		$("#searchBar-2").toggle(300);

	});
});
