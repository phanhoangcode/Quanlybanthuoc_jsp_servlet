<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<title>Hello, world!</title>

<!-- IMPORT COMMON CSS -->
<link rel="import" href="webapp/common/common-css.jsp">

<link rel="stylesheet" href="webapp/css/style.css">
</head>

<body>
	<div class="wrapper">
		<header>
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark my-nav">
				<div class="container-fluid">
					<a href="#" class="navbar-brand ilogo">Pharmacy</a>
					<button class="navbar-toggler" data-toggle="collapse"
						data-target="#navbarText">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse my-items-nav" id="navbarText">
						<!--mr-auto: chỉnh item sang trái -->
						<ul class="navbar-nav mr-auto">
							<li class="nav-item active"><a href="#" class="nav-link">
									<span> <i class="fas fa-heartbeat"></i>&nbsp; <label
										class="form-check-label cursor"> Bán thuốc</label>
								</span>
							</a></li>
							<li class="nav-item dropdown active"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <span> <i
										class="fas fa-file-medical-alt"></i>&nbsp; <label
										class="form-check-label cursor" for="defaultCheck1">
											Hàng hóa </label>
								</span>
							</a>
								<div class="dropdown-menu imenudown"
									aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="#"> <span> <i
											class="fas fa-list-ul"></i>&nbsp; <label
											class="form-check-label cursor"> Danh mục thuốc </label>
									</span>
									</a> <a class="dropdown-item" href="#"> <span> <i
											class="fas fa-dollar-sign"></i>&nbsp; <label
											class="form-check-label cursor"> Thiết lập giá </label>
									</span>
									</a> <a class="dropdown-item" href="#"> <span> <i
											class="fas fa-calendar-check"></i>&nbsp; <label
											class="form-check-label cursor"> Kiểm kho </label>
									</span>
									</a>
								</div></li>
							<li class="nav-item dropdown active"><a
								class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
								role="button" data-toggle="dropdown" aria-haspopup="true"
								aria-expanded="false"> <span> <i
										class="fas fa-exchange-alt"></i>&nbsp; <label
										class="form-check-label cursor" for="defaultCheck1">
											Giao dịch </label>
								</span>
							</a>
								<div class="dropdown-menu imenudown"
									aria-labelledby="navbarDropdown">
									<a class="dropdown-item" href="#"> <span> <i
											class="fas fa-file-alt"></i>&nbsp; <label
											class="form-check-label cursor" for="defaultCheck1">
												Hóa đơn </label>
									</span>
									</a> <a class="dropdown-item" href="#"> <span> <i
											class="fas fa-share-square"></i>&nbsp; <label
											class="form-check-label cursor" for="defaultCheck1">
												Nhập hàng </label>
									</span>
									</a>
								</div></li>
						</ul>
						<ul class="navbar-nav">
							<li class="nav-item dropdown active"><a href="#"
								class="nav-link dropdown-toggle" id="navbarDropdown"
								role="button" data-toggle="dropdown"> <span> <i
										class="fas fa-user-alt"></i>&nbsp; <label
										class="form-check-label" for="defaultCheck1"> Phan
											Hoang </label>
								</span>
							</a>
								<div class="dropdown-menu imenudown-account"
									aria-labelledby="navbarDropdown">
									<a href="#" class="dropdown-item">Thông tin tài khoản</a> <a
										href="#" class="dropdown-item">Xem báo cáo cuối ngày</a> <a
										href="#" class="dropdown-item">Đổi mật khẩu</a>
									<div class="dropdown-divider"></div>
									<a href="#" class="dropdown-item">Đăng xuất</a>
								</div></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<!-- end header-->
		<main class="wrap-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 col-sm-12 col-md-12 col-lg-3 col-xl-2">
					<form action="">
						<article class="search-sb" style="padding-top: 12px;">
							<h6 class="title-sidebar cursor">Tìm kiếm</h6>
							<aside class="">
								<input type="text" class="form-control form-control-sm"
									id="exampleInputEmail1" aria-describedby="emailHelp"
									placeholder="Tìm theo tên thuốc, mã thuốc">
							</aside>
						</article>
						<!-- NHÓM HÀNG -->
						<article class="search-sb" style="padding-top: 12px;">
							<a href="#" class="toggle-search-1">
								<h6 class="title-sidebar cursor">
									Nhóm hàng <i class="fas fa-minus-circle icon-change-1"
										style="float: right;"></i>
								</h6>
							</a>
							<aside id="searchBar-1" class="">
								<input type="text"
									class="form-control form-control-sm input-search-sidebar"
									id="exampleInputEmail1" aria-describedby="emailHelp"
									placeholder="Tìm theo nhóm hàng">
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio1" name="customRadio"
										class="custom-control-input" checked> <label
										class="custom-control-label" for="customRadio1">Tất cả</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio2" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio2">Thuốc
										kháng sinh</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio3" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio3">Thuốc
										bổ</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio4" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio4">Thuốc
										chống viêm</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio5" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio5">Thuốc
										giảm đau</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio6" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio6">Thuốc
										cảm cúm</label>
								</div>
							</aside>
						</article>
						<!-- TỒN KHO -->
						<article class="search-sb" style="padding-top: 12px;">
							<a href="#" class="toggle-search-2">
								<h6 class="title-sidebar cursor">
									Tồn kho <i class="fas fa-minus-circle icon-change-2"
										style="float: right;"></i>
								</h6>
							</a>
							<aside id="searchBar-2" class="">
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio7" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio7">Còn
										hàng trong kho</label>
								</div>
								<div class="custom-control custom-radio input-under-sidebar">
									<input type="radio" id="customRadio8" name="customRadio"
										class="custom-control-input"> <label
										class="custom-control-label" for="customRadio8">Hết
										hàng trong kho</label>
								</div>
							</aside>
						</article>
					</form>
				</div>
				<div class="col-12 col-sm-12 col-md-12 col-lg-9 col-xl-10">
					<div class="d-flex">
						<div class="mr-auto p-2 align-self-center"
							style="text-transform: uppercase;">
							<h5>DANH MỤC THUỐC</h5>
						</div>
						<div class="p-2">
							<button type="button" class="btn btn-sm btn-success"
								data-toggle="modal" data-target="#nhapthuoc"
								data-backdrop="static">
								<i class="fas fa-plus-circle"></i> <label
									class="form-check-label" for="defaultCheck1">Nhập thuốc</label>
							</button>
						</div>
					</div>
					<div class="row" style="padding-top: 10px">
						<div class="col-12">
							<div class="table-responsive-lg table-1">
								<table id="tb-danhsachthuoc"
									class="table table-sm table-hover table-bordered">
									<!-- <table class="table table-sm table-hover"> -->
									<thead>
										<tr class="head-tb">
											<th scope="col" width="10%">Mã thuốc</th>
											<th scope="col" width="15">Tên thuốc</th>
											<th scope="col" width="20%">Nhóm thuốc</th>
											<th scope="col" width="10%">Giá bán</th>
											<th scope="col" width="10%">Giá gốc</th>
											<th scope="col" width="10%">Trạng thái</th>
											<th scope="col" width="10%">Tồn kho</th>
											<th scope="col" width="15%">Hành động</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0101</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-success">Còn hàng</span></td>
											<td>15</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
										<tr>
											<td>QQ0102</td>
											<td>Thuốc ho bổ phế</td>
											<td>Trị ho</td>
											<td>18000</td>
											<td>15000</td>
											<td><span class="badge badge-secondary">Hết hàng</span></td>
											<td>18</td>
											<td>
												<div class="btn-group mr-2" role="group"
													aria-label="First group">
													<button type="button" class="btn btn-info btn-sm">
														<i class="fas fa-pen-square"></i> <label
															class="form-check-label" for="defaultCheck1">Sửa</label>
													</button>
													<button type="button" class="btn btn-danger btn-sm">
														<i class="fas fa-trash-alt"></i> <label
															class="form-check-label" for="defaultCheck1">Xóa</label>
													</button>
												</div>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>

					<!-- 					my- pagin -->
					<!-- 					<div class="d-flex"> -->
					<!-- 						<div class="p-2 ml-auto"> -->
					<!-- 							<nav aria-label="..."> -->
					<!-- 								<ul class="pagination pagination-sm"> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#"> <i -->
					<!-- 											class="fas fa-fast-backward"></i> -->
					<!-- 									</a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#"><i -->
					<!-- 											class="fas fa-step-backward"></i></a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#">1</a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#">2</a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#">3</a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#">4</a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#"><i -->
					<!-- 											class="fas fa-step-forward"></i></a></li> -->
					<!-- 									<li class="page-item"><a class="page-link" href="#"><i -->
					<!-- 											class="fas fa-fast-forward"></i></a></li> -->
					<!-- 								</ul> -->
					<!-- 							</nav> -->
					<!-- 						</div> -->
					<!-- 					</div> -->
				</div>
			</div>
		</div>
		</main>
		<!--end wrap-main-content-->
		<footer class="footer">
			<div class="container container-footer">
				<h6 class="text2">&copy; DIMENSIONMC - DESIGNED BY DEN & FLASH</h6>
			</div>
		</footer>
		<!--end footer-->

		<!-- Modal -->
		<div class="modal fade modal-nhapthuoc" id="nhapthuoc" role="dialog">
			<div class="modal-dialog modal-dialog-centered modal-lg"
				role="document" data-backdrop="static">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle">Nhập
							thuốc</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col-12 col-sm-12 col-md-12 col-lg-8">
									<form>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-12 col-form-label col-form-label-sm label-modal">Mã
												thuốc</label>
											<div class="col-sm-10 col-12 ">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm" placeholder="Nhập mã thuốc">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-form-label col-form-label-sm label-modal">Tên
												thuốc</label>
											<div class="col-sm-10">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm" placeholder="Nhập tên thuốc">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-form-label col-form-label-sm label-modal">Nhóm
												thuốc</label>
											<div class="col-sm-10">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm"
													placeholder="Nhập tên nhóm thuốc: dùng ajax success các nhóm thuốc">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-form-label col-form-label-sm label-modal">Giá
												gốc</label>
											<div class="col-sm-5">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-form-label col-form-label-sm label-modal">Giá
												bán</label>
											<div class="col-sm-5">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-form-label col-form-label-sm label-modal">Tồn
												kho</label>
											<div class="col-sm-5">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm">
											</div>
										</div>
										<div class="form-group row">
											<label for="colFormLabelSm"
												class="col-sm-2 col-md-2 col-form-label col-form-label-sm label-modal">Đơn
												vị</label>
											<div class="col-sm-5 col-md-5">
												<select class="custom-select custom-select-sm">
													<option selected>Lựa chọn đơn vị thuốc: lọ,
														viên...</option>
													<option value="1">Viên</option>
													<option value="2">Lọ</option>
													<option value="3">Gói</option>
													<option value="3">Đơn vị khác</option>
												</select>
											</div>
											<label for="colFormLabelSm"
												class="col-sm-2 col-md-2 col-form-label col-form-label-sm label-modal">Số
												lượng</label>
											<div class="col-sm-3 col-md-3">
												<input type="text" class="form-control form-control-sm"
													id="colFormLabelSm">
											</div>
										</div>
									</form>
								</div>
								<div class="col-12 col-sm-12 col-md-12 col-lg-4">
									<div class="card text-center">

										<div class="card-body">
											<img class="card-img-top"
												src="http://photos.s-cute.com/130901/sample/contents/576_yuna/576_yuna_k04/002.jpg"
												alt="Card image cap">
										</div>
										<div class="card-footer text-muted">
											<a href="#" class="btn btn-sm btn-primary">Thêm ảnh</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer mr-auto">
						<button type="button" class="btn btn-success btn-sm">
							<i class="fas fa-save"></i> <label class="form-check-label"
								for="defaultCheck1">Lưu và thêm mới</label>
						</button>
						<button type="button" class="btn btn-secondary btn-sm"
							data-dismiss="modal">
							<i class="fas fa-ban"></i> <label class="form-check-label"
								for="defaultCheck1">Bỏ qua</label>
						</button>
					</div>
				</div>
			</div>
		</div>


	</div>

	<!-- IMPORT COMMON JS -->
	<link rel="import" href="webapp/common/common-js.jsp">

	<!--MY SCRIPT-->
	<script type="text/javascript" src="webapp/js/script.js"></script>
</body>

</html>
